# README

udv OpenFOAM postProcessing application

Takes into account the effect of acoustic beam broadening on UDV measurements

In order to precisely compare experimental results gained from UDV measurements
to numerical simulations, it is necessary to account for the fact that each
ultrasound pulse broadens spatially as it travels away from the emitting
transducer, thereby reducing the lateral resolution with increasing distance.
Therefore, the measured velocity at each distance interval will be a weighted
average over the radial ﬂuid velocity component (pointing toward or away from
the transducer) within the width of the beam at that distance interval.

## References

Rakan F. Ashour, Douglas H. Kelley, Alejandro Salas, Marco Starace, Norbert Weber, Tom Weier,
Competing forces in liquid metal electrodes and batteries,
Journal of Power Sources, Volume 378, 2018, Pages 301-310,
ISSN 0378-7753,
https://doi.org/10.1016/j.jpowsour.2017.12.042

## Downlaod/Clone main repository

```bash
git clone https://gitlab.hzdr.de/galindo/udv-sensor-openfoam udv_sensor
```

## Compile

```bash
cd udv_sensor/
wmake
```

## Usage

```bash
udv -time 10 -s
```

The parameter “-s” gives a different output of velocities. The sampling point cloud is written into “p.csv”.

The file constant/udv must look like:

```bash
FoamFile { version 2.0; format ascii; class dictionary; location "constant"; object udv; }

// location of sensor
startPoint      (0 -0.044 0.006);
// end of beam
endPoint        (0  0.044 0.006);
// diameter of piezo
D              D [ 0 1 0 0 0 0 0 ] 0.005;
// frequency of sensor
f              f [ 0 0 -1 0 0 0 0 ] 8e6;
// sound velocity
c              c [ 0 1 -1 0 0 0 0 ] 2730;
// number of sample points
n              3;
```

## Explanation

For each sampling point on the UDV beam axis, a point cloud is produced. All
cells touching this cloud are selected. The mean velocity, weighted by UDV
intensity and cell volume is computed.

readData.H

- read udv file
- the first sampling point is excluded, because it is directly at the UDV sensor
- calculate direction of beam in spherical coordinates
- calculate wave length, near field length and half angle of beam

udv.C

- compute for all sampling points on beam axis: coordinate, distance to sensor
- compute minimal cell lenght - the cloud point distance is one half
- compute angle between sensor and all points
- compute weighting factor for all cells (near field weighting factor is set to 1 for all cells in the cylinder)

For each sampling point on the beam axis:

- construct point cloud in spherical coordinates
- transform point cloud to xyz coordinate system
- write point cloud to file
- findCell for all cloud points
- velocity & normal vector are weighted by intensity and cell volume

